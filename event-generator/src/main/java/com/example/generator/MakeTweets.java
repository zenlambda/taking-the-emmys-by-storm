package com.example.generator;

import java.net.UnknownHostException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import net.sourceforge.jaulp.random.date.RandomDateUtils;

import org.joda.time.Interval;
import org.joda.time.format.PeriodFormat;
import org.joda.time.format.PeriodFormatter;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class MakeTweets {
	
	private static String[] KEYWORDS = new String[] {
		"glee","msleamichele","colfer",
		"#got","thrones",
		"modern family", "modernfamily", "burrell", "bowen",
		"mildren","winslet","pierce",
		"big bang theory","big bang","sheldon","parsons",
		"office","carell","dunder","mifflin",
		"snl","saturday night live",
		"parks","poehler","recreation",
		"daily show","stewart",
		"colbert",
		"lynch"
	};

	public static void main(String[] args) throws UnknownHostException,
			MongoException {
		Random rand = new Random();
		long startTime = new Date().getTime();
		Mongo m = new Mongo();

		try {
			SimpleDateFormat sdf = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss.SSSZ");

			NumberFormat format = NumberFormat.getInstance();
			format.setMinimumIntegerDigits(18);
			format.setGroupingUsed(false);
			Calendar cal = Calendar.getInstance();
			cal.set(2011, Calendar.SEPTEMBER, 18);
			Date start = cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, 2);
			Date end = cal.getTime();

			DB db = m.getDB("evntcast_db");

			DBCollection coll = db.getCollection("tweets");
			coll.remove(new BasicDBObject());

			// long limit = 1000000000000000000L;
			long limit = 1000;
			for (long i = 1; i <= limit; i++) {

				BasicDBObject tweet = new BasicDBObject();
				tweet.put("id", i);

				Date creationTime = RandomDateUtils
						.createRandomDatebetween(start, end);
				
				tweet.put("created_at", sdf.format(creationTime));
				tweet.put("creation_time", creationTime);
				tweet.put("text", KEYWORDS[rand.nextInt(KEYWORDS.length)]);
				
				

				coll.insert(tweet);
				System.out.printf("inserted %d tweets \n", i);
			}
			long endTime = new Date().getTime();
			Interval executionTime = new Interval(startTime, endTime);
			PeriodFormatter pformat = PeriodFormat.getDefault();
			System.out.println("in " + pformat.print(executionTime.toPeriod()));
		} finally {
			m.close();
		}
	}

}
