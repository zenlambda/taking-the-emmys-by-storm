package com.example.evntcast.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

public class ReplayServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		long startTime = new Date().getTime();
		Mongo m = new Mongo();
		PrintWriter out = resp.getWriter();
		try {
			DB db = m.getDB("evntcast_db");

			DBCollection coll = db.getCollection("tweets");
			
			@SuppressWarnings("serial")
			DBCursor cursor = coll.find(new BasicDBObject(),new BasicDBObject(
					new HashMap<Object,Object>(){{
				put("_id", 0);
				put("creation_time", 0);
			}})).sort(new BasicDBObject(new HashMap<Object,Object>() {{
				put("creation_time",1);
			}}));
			
			
			while(cursor.hasNext()) {
				DBObject obj = cursor.next();
				out.println(obj.toString());
				out.flush();
			}
			
		} finally {
			m.close();
		}
		
	}

}
