# Storm Demo: 'Taking the Emmys by Storm'

This is some code I wrote after watching a screencast
'taking-the-emmys-by-storm' screencast by Adrian Petrescu[1]
back in September 2011.

My contributions here are an open source substitute for the
'evntcast-server' component for serving a simulated twitter feed generated
by 'event-generator'. MongoDB is used to store the generated tweets.

This repo does not contain any of Adrian's code. His screencast code would go
under 'storm-starter' and could be reproduced by starting from a
(2011 version of) 'storm-starter' project [2], watching the video closely
and making changes accordingly.

If all the code is brought together the screencast can be reproduced by
running the even-generator to populate MongoDB with the simulated tweet data,
starting 'evntcast-server' and then running the storm topology.

Please note: Storm has moved on a lot so this code is probably out of
date and would probably not be a good starting point for a beginner.

[1] http://storm.twitsprout.com/

[2] https://github.com/nathanmarz/storm-starter
